package com.example.currencyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public EditText userCurrencyValue;


    public RadioButton calculateBuyPrice;
    public RadioButton calculateSellPrice;

    public CheckBox usaCurrency;
    public CheckBox eurCurrency;
    public CheckBox ruCurrency;
    public CheckBox kzCurrency;

    public TextView textForCheckBoxUsaCurrency;
    public TextView textForCheckBoxEurCurrency;
    public TextView textForCheckBoxRuCurrency;
    public TextView textForCheckBoxKzCurrency;

    public TextView convertUsdResult;
    public TextView convertEurResult;
    public TextView convertRuResult;
    public TextView convertKzResult;

    private float usaSell = 74.12f;
    private float eurSell = 83.23f;
    private float ruSell = 1.15f;
    private float kzSell = 0.22f;

    private float usaBuy = 76.33f;
    private float eurBuy = 81.11f;
    private float ruBuy = 1.05f;
    private float kzBuy = 0.18f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userCurrencyValue = findViewById(R.id.userCurrencyValue);

        userCurrencyValue.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    checkCheckBoxesAndCalculate();
                } else {
                    convertUsdResult.setText("0");
                    convertEurResult.setText("0");
                    convertRuResult.setText("0");
                    convertKzResult.setText("0");
                }
            }
        });

        calculateBuyPrice = findViewById(R.id.buyCurrencyRadioButton);
        calculateSellPrice = findViewById(R.id.sellCurrencyRadioButton);

        usaCurrency = findViewById(R.id.checkBoxCurrencyUsa);
        eurCurrency = findViewById(R.id.checkBoxCurrencyEur);
        ruCurrency = findViewById(R.id.checkBoxCurrencyRu);
        kzCurrency = findViewById(R.id.checkBoxCurrencyKz);

        textForCheckBoxUsaCurrency = findViewById(R.id.textForCheckBoxCurrencyUsa);
        textForCheckBoxEurCurrency = findViewById(R.id.textForCheckBoxCurrencyEur);
        textForCheckBoxRuCurrency = findViewById(R.id.textForCheckBoxCurrencyRus);
        textForCheckBoxKzCurrency = findViewById(R.id.textForCheckBoxCurrencyKz);

        convertUsdResult = findViewById(R.id.convertUsdResult);
        convertEurResult = findViewById(R.id.convertEurResult);
        convertRuResult = findViewById(R.id.convertRusResult);
        convertKzResult = findViewById(R.id.convertKzResult);

        calculateBuyPrice.setOnClickListener(this);
        calculateSellPrice.setOnClickListener(this);

        textForCheckBoxUsaCurrency.setOnClickListener(this);
        textForCheckBoxEurCurrency.setOnClickListener(this);
        textForCheckBoxRuCurrency.setOnClickListener(this);
        textForCheckBoxKzCurrency.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.textForCheckBoxCurrencyUsa:
                usaCurrency.setChecked(!usaCurrency.isChecked());
                break;
            case R.id.textForCheckBoxCurrencyEur:
                eurCurrency.setChecked(!eurCurrency.isChecked());
                break;
            case R.id.textForCheckBoxCurrencyRus:
                ruCurrency.setChecked(!ruCurrency.isChecked());
                break;
            case R.id.textForCheckBoxCurrencyKz:
                kzCurrency.setChecked(!kzCurrency.isChecked());
                break;
        }
        checkCheckBoxesAndCalculate();
    }

    private void showConvertResult(CheckBox currencyChoose, TextView currencyConvertResultView, float userValue, float currency) {
        if (currencyChoose.isChecked()) {
            currencyConvertResultView.setText(" " + userValue / currency + " ");
        } else {
            currencyConvertResultView.setText("0");
        }
    }

    public void checkCheckBoxesAndCalculate() {
        String userValueString = userCurrencyValue.getText().toString();
        float userValue;

        if (!userValueString.isEmpty()) {
            userValue = Float.parseFloat(userValueString);


            if (calculateBuyPrice.isChecked()) {
                showConvertResult(usaCurrency, convertUsdResult, userValue, usaBuy);

                showConvertResult(eurCurrency, convertEurResult, userValue, eurBuy);

                showConvertResult(ruCurrency, convertRuResult, userValue, ruBuy);

                showConvertResult(kzCurrency, convertKzResult, userValue, kzBuy);
            } else {
                showConvertResult(usaCurrency, convertUsdResult, userValue, usaSell);

                showConvertResult(eurCurrency, convertEurResult, userValue, eurSell);

                showConvertResult(ruCurrency, convertRuResult, userValue, ruSell);

                showConvertResult(kzCurrency, convertKzResult, userValue, kzSell);
            }
        }
    }
}
